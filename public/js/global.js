/**
 * @name 判断数据是否为空
 * @param string|false|object
 * @return boolean
 * */
function empty (mixed_var) {
    var undef, key, i, len;
    var emptyValues = [undef, null, false, 0, '', '0'];
    for (i = 0, len = emptyValues.length; i < len; i++) {
        if (mixed_var === emptyValues[i]) {
            return true;
        }
    }

    if (typeof mixed_var === 'object') {
        for (key in mixed_var) {
            // TODO: should we check for own properties only?
            // if (mixed_var.hasOwnProperty(key)) {
            return false;
            // }
        }
        return true;
    }

    return false;
}
/**
 * @name 模拟 PHP explode 函数，切割字符串维数组
 * @param
 * @return boolean
 * */
function explode(delimiter, string, limit) {
    if (arguments.length < 2 || typeof delimiter === 'undefined' || typeof string === 'undefined') return null;
    if (delimiter === '' || delimiter === false || delimiter === null) return false;
    if (typeof delimiter === 'function' || typeof delimiter === 'object' || typeof string === 'function' || typeof string ===
        'object') {
        return {
            0: ''
        };
    }
    if (delimiter === true) delimiter = '1';

    // Here we go...
    delimiter += '';
    string += '';

    var s = string.split(delimiter);

    if (typeof limit === 'undefined') return s;

    // Support for limit
    if (limit === 0) limit = 1;

    // Positive limit
    if (limit > 0) {
        if (limit >= s.length) return s;
        return s.slice(0, limit - 1)
            .concat([s.slice(limit - 1)
                .join(delimiter)
            ]);
    }

    // Negative limit
    if (-limit >= s.length) return [];

    s.splice(s.length + limit);
    return s;
}
/**
 * @name 字符串转为数字
 * @param string|number mixed_var
 * @return number
 * */
function intval(mixed_var, base) {
    var tmp;
    var type = typeof mixed_var;
    if (type === 'boolean') {
        return +mixed_var;
    } else if (type === 'string') {
        tmp = parseInt(mixed_var, base || 10);
        return (isNaN(tmp) || !isFinite(tmp)) ? 0 : tmp;
    } else if (type === 'number' && isFinite(mixed_var)) {
        return mixed_var | 0;
    } else {
        return 0;
    }
}

function implode(glue, pieces) {
    var i = '',
        retVal = '',
        tGlue = '';
    if (arguments.length === 1) {
        pieces = glue;
        glue = '';
    }
    if (typeof pieces === 'object') {
        if (Object.prototype.toString.call(pieces) === '[object Array]') {
            return pieces.join(glue);
        }
        for (i in pieces) {
            retVal += tGlue + pieces[i];
            tGlue = glue;
        }
        return retVal;
    }
    return pieces;
}
function is_array(mixed_var) {
    var ini,
        _getFuncName = function(fn) {
            var name = (/\W*function\s+([\w\$]+)\s*\(/)
                .exec(fn);
            if (!name) {
                return '(Anonymous)';
            }
            return name[1];
        };
    _isArray = function(mixed_var) {
        if (!mixed_var || typeof mixed_var !== 'object' || typeof mixed_var.length !== 'number') {
            return false;
        }
        var len = mixed_var.length;
        mixed_var[mixed_var.length] = 'bogus';
        if (len !== mixed_var.length) { // We know it's an array since length auto-changed with the addition of a
            mixed_var.length -= 1;
            return true;
        }
        delete mixed_var[mixed_var.length];
        return false;
    };

    if (!mixed_var || typeof mixed_var !== 'object') {
        return false;
    }
    this.php_js = this.php_js || {};
    this.php_js.ini = this.php_js.ini || {};
    ini = this.php_js.ini['phpjs.objectsAsArrays'];

    return _isArray(mixed_var) ||
        ((!ini || ( // if it's not set to 0 and it's not 'off', check for objects as arrays
            (parseInt(ini.local_value, 10) !== 0 && (!ini.local_value.toLowerCase || ini.local_value.toLowerCase() !==
                'off')))) && (
            Object.prototype.toString.call(mixed_var) === '[object Object]' && _getFuncName(mixed_var.constructor) ===
            'Object' // Most likely a literal and intended as assoc. array
        ));
}
function in_array(needle, haystack, argStrict) {
    var key = '',
        strict = !! argStrict;
    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle) {
                return true;
            }
        }
    } else {
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }
    }
    return false;
}
function is_empty(msg){
    return !/.+/.test(msg);
}
function time () {
    return Math.floor(new Date()
        .getTime() / 1000)
}
function debug(msg){
    console.log(msg);
}
function log(msg) {
    debug(msg);
}
function base_url() {
    return window.location.protocol+'//'+window.location.host;
}
function redirect(path) {
    var host = base_url();
    window.location.href= host+path;
}
function json_encode(obj){
    return JSON.stringify(obj)
}

