<?php
namespace App\Service;
/** 
 图片压缩操作类 
 v1.0 
*/
/* *
 * 使用示例：
 * 压缩图片
 * 裴纪阳
 * @param String $src 图片原路径
 * @param float $percent 压缩百分比
 * 2017-03-30
 *
function ImageCompression($src,$percent=1){
    vendor('Image.ImageCompression');
    $img = new ImageCompression($src, $percent);
    $type = new \Think\Image(1, $src);
    $type -> type();
    $path = substr($src,0, strrpos($src, '/'));
    do{
        $filename = 'ys'.date('YmdHis').mt_rand(10000000,99999999);
    }while(file_exists($path.'/'.$filename));
    $img -> thumpImage();
    $img -> saveImage($path.'/'.$filename);
    return $path.'/'.$filename.'.'.$type -> type();

}
 * */
class ImageCompression{
         
       private $src;  
       private $imageinfo;  
       private $image;
       private $percent;
       public function __construct($src,$percent){
           $this -> percent = $percent;
           $this->src = $src;  
           $this -> openImage();
       }  
       /** 
       打开图片 
       */  
       public function openImage(){  
             
           list($width, $height, $type, $attr) = getimagesize($this->src);  
           $this->imageinfo = array(  
                  
                'width'=>$width,  
                'height'=>$height,  
                'type'=>image_type_to_extension($type,false),  
                'attr'=>$attr  
           );
           $fun = "imagecreatefrom".$this->imageinfo['type'];// 图片类型函数imagecreatefrom  jpeg|png|gif
           $this->image = $fun($this->src);
       }  
       /** 
       操作图片 
       */  
       public function thumpImage(){  
             
            $new_width = $this->imageinfo['width'] * $this->percent;  
            $new_height = $this->imageinfo['height'] * $this->percent;  
            $image_thump = imagecreatetruecolor($new_width,$new_height);  
            //将原图复制带图片载体上面，并且按照一定比例压缩,极大的保持了清晰度
            if(function_exists('imagecopyresampled')){
                imagecopyresampled($image_thump,$this->image,0,0,0,0,$new_width,$new_height,$this->imageinfo['width'],$this->imageinfo['height']);
            }else{
                imagecopyresized($image_thump,$this->image,0,0,0,0,$new_width,$new_height,$this->imageinfo['width'],$this->imageinfo['height']);
            }
            imagedestroy($this->image);
            $this->image =   $image_thump;
       }  
       /** 
       输出图片 
       */  
       public function showImage(){  
             
            header('Content-Type: image/'.$this->imageinfo['type']);  
            $funcs = "image".$this->imageinfo['type'];  
            $funcs($this->image);  
             
       }  
       /** 
       保存图片到硬盘 
       */  
       public function saveImage($name){  
             
            $funcs = "image".$this->imageinfo['type'];
            $funcs($this->image,$name.'.'.$this->imageinfo['type']);
                
       }  
       /** 
       销毁图片 
       */  
       public function __destruct(){  
             
           imagedestroy($this->image);  
       }  
         
   }
?>  
